
object RemoteNoteRepositoryImpl: INoteRepository {
    override fun getNote(): NoteResult = NoteResult.Success("This is required data from remote") // syntax is same as local, but the simplified one
}