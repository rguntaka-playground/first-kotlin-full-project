const val GENERIC_ERROR = "Error occured"

// Application Container
//1. Create things of program
//2. Bind things of program
//3. Set program in motion

/*
 whatever the framework MVC, MVM, MVVM, etc, the program should handle View, Data, Logic in an organised manner
 */
fun main(args: Array<String>){
    val data = NoteRepositoryImpl(LocalNoteRepositoryImpl, RemoteNoteRepositoryImpl)
    val logic = NoteLogic()
    val view = NoteView(logic)

    logic.repository = data
    logic.view = view

    view.clickEvent()

}