

/*
One of these two shall be the outcome to be displayed on view
 Success or Error
 */

sealed class NoteResult{
    object Error: NoteResult()
    data class Success(val noteContents: String): NoteResult()
}