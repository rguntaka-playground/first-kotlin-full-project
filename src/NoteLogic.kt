
/*
Logic process the things and map data to view as needed
 In the process, it handles the events triggered from Views
 It triggers call to Repository that fetch data and map it with view
 */
class NoteLogic: INoteContract.Logic{
    lateinit var repository: INoteRepository
    lateinit var view: INoteContract.View

    override fun handleEvent(event: ViewEvent) {
        when(event){
            is ViewEvent.OnStart -> getData()
            //is ViewEvent.OnClose -> doBeforeClose()
        }
    }

    fun getData(){
        val result = repository.getNote()

        when(result){
            is NoteResult.Success -> view.displayResult(result.noteContents)
            is NoteResult.Error -> view.displayResult(GENERIC_ERROR)
        }
    }
}