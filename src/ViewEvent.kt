
/*
  This sealed class (similar to the abstract class but with flexibility) declare all possible events of a View
  These are implemented in Logic class as it handles the Events from the View
 */

sealed class ViewEvent {
    object OnStart: ViewEvent()
    object OnClose: ViewEvent ()
}