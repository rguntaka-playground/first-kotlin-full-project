
// contract interface
interface INoteContract {
    interface View {
        fun displayResult(result: String) // function stub, i.e. definition without body
    }

    interface Logic{
        fun handleEvent(event: ViewEvent)
    }
}




