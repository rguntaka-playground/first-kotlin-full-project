
class NoteRepositoryImpl(
    val localDatabase: INoteRepository,
    val remoteDatabase: INoteRepository
): INoteRepository{
    override fun getNote(): NoteResult {
        val remoteResult = remoteDatabase.getNote()

        return when(remoteResult){
            is NoteResult.Success ->  remoteResult
            is NoteResult.Error -> localDatabase.getNote()
        }
    }
}
