
/*
Class that represents the View
 It gets the data via Logic and display data for users
 It binds events to the logic for further handling
 */

class NoteView(val logic: INoteContract.Logic): INoteContract.View{
    override fun displayResult(result: String) {
        TODO("Not yet implemented")
        println(result)
    }

    fun clickEvent() = logic.handleEvent(ViewEvent.OnStart)
}
